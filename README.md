# F5刷新本頁，请Ctrl+D收藏本网页地址，确保永久访问

## 永久回家地址：
### https://huijia.gitbook.io/fuliya/

## 最新地址：
### https://fuliya.site/

## 回家郵箱：（發送任意字符，回復最新回家地址）
### fuliyaxyz@gmail.com

## 加速器：
### 白鯨加速器：https://www.bjch999.com/?mid=3004
### 螞蟻加速器：https://29.obqyyh.com/c-1603/a-aPTyb

### 推荐Alook浏览器：https://www.alookweb.com/


# ✉ 获取最新地址
### https://atg2022.gitbook.io/home/
### https://bitbucket.org/atgcc/atgcc
### https://gitlab.com/atg888/home
### 发邮件给 [wwwatgcc@gmail.com](mailto:wwwatgcc@gmail.com) ，会自动回复包含最新地址信息的邮件


# ✐ 温馨提示
推荐使用谷歌(Chrome)浏览器访问本站，谷歌浏览器速度更快，iPhone建议使用手机自带Safria浏览器访问。
如果您记不住本站域名，请收藏该页地址，收藏并分享给好友。推薦瀏覽器：https://www.alookweb.com/
1、使用电脑的用户，请按键盘上的CTRL+D进行收藏。
2、苹果手机用户再浏览器点击底部导航栏中间图标，然后添加到个人收藏或主屏幕。
3、安卓手机用户点击，或者打开浏览器设置，添加到书签或主屏幕。
